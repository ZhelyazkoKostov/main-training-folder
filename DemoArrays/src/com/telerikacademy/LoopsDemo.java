package com.telerikacademy;

import java.util.ArrayList;

public class LoopsDemo {
    public static void main(String[] args) {
        ArrayList<Integer> list = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            list.add(1);
        }

        for (int i = 0; i < 5; i++) {
            list.set(i, list.get(i) + i);
        }

        for (Integer number : list) {
            System.out.println(number);
        }

        while (list.size() > 3) {
            list.remove(list.size() - 1);
        }

        for (int i = 0; i < list.size(); i++) {
            if (i % 2 == 0) {
                continue;
            }

            System.out.println("odd : " + list.get(i));
        }

        for (int i = 0; i < list.size(); i++) {
            System.out.println(list.get(i));
            if (list.get(i) == 2) {
                break;
            }
        }
    }
}
