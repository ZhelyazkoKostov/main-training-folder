package com.telerikacademy;

import java.util.ArrayList;

public class ArrayListDemo {
    public static void main(String[] args) {
        ArrayList<Integer> list = new ArrayList<>();

        for (int i = 6; i > 0; i--) {
            list.add(i);
        }
       // list.forEach(System.out::println);

        list.set(0, 32);

        System.out.println(list.size());

        System.out.println(list.get(list.size() - 1));

        for (int i = 0; i < list.size(); i++) {
            System.out.printf("%d ", list.get(i));
        }
    }
}
