package com.telerikacademy;

public class DemoArrays {
    public static void main(String[] args) {
//        int [][] matrix = new int[2][2];
//        matrix[0][0] = 1;
//        matrix[0][1] = 2;
//        matrix[1][0] = 3;
//        matrix[1][1] = 4;
//
//        int[][] matrix2 = {
//                {1, 2},
//                {3, 4}
//        };
//
//        matrix[matrix.length - 1][matrix[0].length - 1] = 10;

        int[][] matrix1 = new int[5][3];

        int[][] matrix2 = {
                {1, 2, 3},
                {4, 5, 6},
                {7, 8, 9}
        };

        matrix2[1][2] = 20;

        System.out.println(matrix2[matrix2.length - 1][matrix2[0].length - 1]);

        for (int row = 0; row < matrix2.length; row++) {
            for (int col = 0; col < matrix2[0].length; col++) {
                System.out.print(matrix2[row][col]);
                System.out.print(" ");
            }
            System.out.println();
        }
    }
}
