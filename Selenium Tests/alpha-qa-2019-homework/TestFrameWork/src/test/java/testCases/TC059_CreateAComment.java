package testCases;

import org.junit.Before;
import org.junit.Test;

public class TC059_CreateAComment extends BaseTest {

    String comment = "Replying to a post";

    @Before
    public void logInSet(){
        actions.logIn();
        actions.signIn();
    }
    @Test
    public void createACommentForExistingTopic(){
        //Find topic in forum
        actions.waitForElementVisible("xpath.searchFirstTopicFromMainList", 10);
        actions.clickElement("xpath.searchFirstTopicFromMainList");

        //Create a comment
        actions.waitForElementVisible("xpath.replyOnCommentButton", 10);
        actions.clickElement("xpath.replyOnCommentButton");
        actions.waitForElementVisible("xpath.createTopicTextArea", 10);
        actions.typeValueInField(comment, "xpath.createTopicTextArea");

        //Assert comment is created
        actions.clickElement("xpath.replyAreaCreateTopicButton");
        actions.assertElementNotPresent("xpath.cancelReplyOnCommentButton");
    }
}

