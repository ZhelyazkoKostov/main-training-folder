package testCases;
import com.telerikacademy.testframework.UserActions;
import com.telerikacademy.testframework.Utils;
import org.junit.Assert;
import org.junit.Test;

import java.util.concurrent.TimeUnit;

public class TC0091_ShareIfNotLogIn extends BaseTest{
    @Test
    public void ShareIfNotLogIn(){

        //delete all cookie
        Utils.getWebDriver().manage().deleteAllCookies();
        //dynamic wait
        Utils.getWebDriver().manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        UserActions.loadBrowser();

        actions.clickElement("xpath.clickOnTheTopic");
        actions.clickElement("xpath.ClickShareButton");

        Assert.assertEquals("https://schoolforum.telerikacademy.com/t/o3dlbs4aq5/554",
                "https://schoolforum.telerikacademy.com/t/o3dlbs4aq5/554");
    }
}
