package testCases;

import com.telerikacademy.testframework.UserActions;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;

public class BaseTest {
	UserActions actions = new UserActions();

	@Before
	public void logInForum(){
		actions.logIn();
		actions.signIn();
	}


	@BeforeClass
	public static void setUp(){
		UserActions.loadBrowser();


	}

//	@AfterClass
//	public static void tearDown(){
//		UserActions.quitDriver();
//	}
}
