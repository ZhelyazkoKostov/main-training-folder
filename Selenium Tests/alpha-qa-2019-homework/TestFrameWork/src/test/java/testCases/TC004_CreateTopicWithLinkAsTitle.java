package testCases;

import org.junit.Test;

public class TC004_CreateTopicWithLinkAsTitle extends BaseTest {

    @Test
    public void createTopicWithLinkAsTitle() {
        String websiteName = actions.getRandomString(10);
        String firstTitlePart = "www.";
        String lastTitlePart = ".com";

        String topicTitle = firstTitlePart + websiteName + lastTitlePart;
        String topicMainText = "TC04 - Create a new topic with link as a title.";

        actions.logIn();
        actions.signIn();

        actions.clickNewTopicButton();
        actions.inputNewTopicTitle(topicTitle);
        actions.inputNewTopicMainText(topicMainText);
        actions.clickCreateNewTopicButton();

        actions.checkNewlyCreatedTopicTitleExist(topicTitle);
    }
}
