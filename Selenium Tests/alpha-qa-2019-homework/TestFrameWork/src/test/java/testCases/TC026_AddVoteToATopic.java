package testCases;

import org.junit.Assert;
import org.junit.Test;

public class TC026_AddVoteToATopic extends BaseTest {
    @Test
    public void VerifyAddVoteToTopicIsWorking(){

        final String VOTES_STATUS_LIMITED = "Limit";
        final String VOTES_STATUS_VOTED = "Voted";

        String topic = actions.createUniqueTopic();

        //Create a Topic
        actions.logIn();
        actions.signIn();

        //create new topic
        actions.clickNewTopicButton();
        actions.inputNewTopicTitle(topic);
        actions.inputNewTopicMainText(topic);
        actions.clickElement("xpath.replyAreaCreateTopicButton");
        actions.waitForElementVisible("//div[@class='vote-count']", 10);

        //get votes before voting

        String currentVotesStatus = actions.getText("xpath.votesStatus");

        if (!currentVotesStatus.equals(VOTES_STATUS_LIMITED) &&
                !currentVotesStatus.equals(VOTES_STATUS_VOTED)){
            int votesBeforeVoting = Integer.parseInt(actions.getText("xpath.votesForATopic"));
            actions.clickElement("xpath.voteButton");
            int votesAfterVoting = Integer.parseInt(actions.getText("xpath.votesForATopic"));
            int differenceInVotes = votesAfterVoting- votesBeforeVoting;
            Assert.assertTrue(differenceInVotes == 1);
        }
    }
}
