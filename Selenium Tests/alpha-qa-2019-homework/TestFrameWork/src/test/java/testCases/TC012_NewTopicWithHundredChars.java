package testCases;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class TC012_NewTopicWithHundredChars extends BaseTest{

    //Zhelyazko

    String title = actions.createUniqueTopic()+"Lorem Ipsum is simply dummy text of the printing and typesetting e";
    String text = "text popular Ipsum is not simply random belief, Lorem ";


    @Before
    public void logInSet(){
        actions.logIn();
        actions.signIn();

    }

    @Test
    public void TC12_verifyNewTopicWithHundredCharsCreated(){
        actions.clickNewTopicButton();
        actions.clearField("xpath.createTopicTitleField");
        actions.inputNewTopicTitle(title);
        actions.inputNewTopicMainText(text);
        actions.clickCreateNewTopicButton();
        actions.returnToHomePage();
        String createdTopicTitle = title;
        String actualTopicTitle = actions.checkNewlyCreatedTopicTitleExist(title);
        Assert.assertEquals(createdTopicTitle,actualTopicTitle);
    }
}
