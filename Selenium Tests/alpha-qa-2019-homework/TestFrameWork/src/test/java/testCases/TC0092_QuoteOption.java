package testCases;
import com.telerikacademy.testframework.Utils;
import org.junit.Assert;
import org.junit.Test;

import java.util.concurrent.TimeUnit;

public class TC0092_QuoteOption extends BaseTest {
    @Test

    public void QuoteOption()  {
        //delete all cookie
        Utils.getWebDriver().manage().deleteAllCookies();
        //dynamic wait
        Utils.getWebDriver().manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

        actions.logIn();
        actions.signIn();
        actions.clickElement("xpath.clickOnTheTopic");
        actions.clickElement("xpath.replayButtonIfUserIsLogIn");

        Assert.assertEquals("Hello", "Hello");
    }
}
