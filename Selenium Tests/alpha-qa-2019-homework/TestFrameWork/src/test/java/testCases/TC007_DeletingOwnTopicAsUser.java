package testCases;

import org.junit.Test;

public class TC007_DeletingOwnTopicAsUser extends BaseTest {

    @Test
    public void deletingOwnTopicAsUser() {
        String topicTitle = actions.getRandomString(10);
        String topicMainText = actions.getRandomString(30);

        actions.logIn();
        actions.signIn();

        actions.clickNewTopicButton();
        actions.inputNewTopicTitle(topicTitle);
        actions.inputNewTopicMainText(topicMainText);
        actions.clickCreateNewTopicButton();

        actions.deleteTopic();
    }
}