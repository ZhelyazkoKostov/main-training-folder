package testCases;

import com.telerikacademy.testframework.Utils;
import org.junit.Assert;
import org.junit.Test;
import static com.telerikacademy.testframework.Utils.getConfigPropertyByKey;



public class TCs extends BaseTest{

    @Test
    public void TC001_CreateATopicAsAUser() {
        String topic = actions.createUniqueTopic();

        actions.clickElement("xpath.createTopicButton"); // Click "New Topic" button
        actions.inputNewTopicTitle(topic);
        actions.inputNewTopicMainText(topic);
        actions.clickElement("xpath.replyAreaCreateTopicButton");
        actions.waitForElementVisible("//*[@id=\"topic-title\"]/div/div/h1/a", 10);
        actions.assertElementPresent("//*[@id=\"topic-title\"]/div/div/h1/a");

    }

    @Test
    public void TC002_EditTopicAsRegisteredUser(){

        //navigate to user dropdown panel
        Utils.LOG.info("Navigating to user dropdown panel");
        actions.waitForElementVisible("xpath.currentUserIcon", 5);
        actions.clickElement("xpath.currentUserIcon");
        actions.clickElement("xpath.currentUserIconInUserPanel");

        //select an already created topic
        Utils.LOG.info("Selecting an already created random topic");
        actions.waitForElementVisible("xpath.topicsIconInUserOptions", 5);
        actions.clickElement("xpath.topicsIconInUserOptions");
        actions.clickElement("xpath.randomTopicCreatedByRegisteredUser");

        //edit topic's title
        String editedTitle = "Test " + actions.getRandomString(20);
        Utils.LOG.info("Editing topic title: " + editedTitle);
        actions.waitForElementVisible("xpath.editTopicIcon", 5);
        actions.clickElement("xpath.editTopicIcon");
        actions.clickElement("xpath.editTopicTitleField");
        actions.clearFieldForMacOS("xpath.editTopicTitleField");
        actions.clearField("xpath.editTopicTitleField");
        actions.typeValueInField(editedTitle, "xpath.editTopicTitleField");


        //Select category
        Utils.LOG.info("Selecting a category");
        actions.selectCategory();
        actions.clickElement("//button[@class=\"btn-primary submit-edit btn no-text btn-icon ember-view\"]");

        //Assert topic's title is edited
        String editedTopicTitle = editedTitle;
        String actualEditedTopicTitle = actions.checkEditedTopicTitleExist(editedTitle);
        Assert.assertEquals(editedTopicTitle,actualEditedTopicTitle);

        //Assert category is selected
        actions.assertElementPresent("xpath.siteFeedbackCategory");

    }

    @Test
    public void TC003_CreateTopicAsNotRegisteredUser() {
        actions.logOut();
        actions.waitForElementVisible("xpath.logInForumButton", 4);
        actions.assertElementNotPresent("xpath.createTopicButton");
    }

    @Test
    public void TC004_CreateTopicWithLinkAsTitle() {
        String websiteName = actions.getRandomString(10);
        String firstTitlePart = "www.";
        String lastTitlePart = ".com";

        String topicTitle = firstTitlePart + websiteName + lastTitlePart;
        String topicMainText = "TC04 - Create a new topic with link as a title.";


        actions.clickNewTopicButton();
        actions.inputNewTopicTitle(topicTitle);
        actions.inputNewTopicMainText(topicMainText);
        actions.clickCreateNewTopicButton();

        actions.checkNewlyCreatedTopicTitleExist(topicTitle);
    }

    @Test
    public void TC005_CreateANewTopicWithCategory() {
        String topicTitle = actions.getRandomString(10);
        String topicMainText = actions.getRandomString(30);

        actions.clickNewTopicButton();
        actions.inputNewTopicTitle(topicTitle);
        actions.inputNewTopicMainText(topicMainText);
        actions.clickElement("xpath.categoryDropDown");
        actions.clickElement("xpath.siteFeedbackCategory");
        actions.clickCreateNewTopicButton();
    }

    @Test
    public void TC006_CreateATopicWithoutTitleAndText() {
        actions.clickNewTopicButton();
        actions.clickCreateNewTopicButton();

        actions.assertElementPresent("xpath.TitleRequiredWarningMsg");
        actions.assertElementPresent("xpath.postCantBeEmptyWarningMsg");
    }

    @Test
    public void TC007_DeletingOwnTopicAsUser() {
        String topicTitle = actions.getRandomString(10);
        String topicMainText = actions.getRandomString(30);

        actions.clickNewTopicButton();
        actions.inputNewTopicTitle(topicTitle);
        actions.inputNewTopicMainText(topicMainText);
        actions.clickCreateNewTopicButton();

        actions.deleteTopic();

    }

    @Test
    public void TC007_DeletingOwnTopicAsUserCHANGED(){

        //navigate to user dropdown panel
        Utils.LOG.info("Navigating to user dropdown panel");
        actions.waitForElementVisible("xpath.currentUserIcon", 5);
        actions.clickElement("xpath.currentUserIcon");
        actions.clickElement("xpath.currentUserIconInUserPanel");

        //select an already created topic
        Utils.LOG.info("Selecting an already created random topic");
        actions.waitForElementVisible("xpath.topicsIconInUserOptions", 5);
        actions.clickElement("xpath.topicsIconInUserOptions");
        actions.clickElement("xpath.randomTopicCreatedByRegisteredUser");

        //delete the created topic
        Utils.LOG.info("Delete the selected topic");
        actions.deleteTopicZK();
    }

    @Test
    public void TC009_CreateNewTopicWith4CharsInTitleField() {
        actions.clickNewTopicButton();
        actions.inputNewTopicTitle(getConfigPropertyByKey("topicTitle4charsInput"));
        actions.inputNewTopicMainText(getConfigPropertyByKey("textbox11charsInput"));
        actions.clickNewTopicButtonInPopup();
        actions.assertElementPresent("xpath.topicTitleLengthOutOfBoundaryAlert");
    }

    @Test
    public void TC010_CreateTopicWithFiveTitleChars() {
        String title = actions.getRandomString(5);
        String comment = actions.getRandomString(50);

        actions.clickNewTopicButton();

        //Creating title
        Utils.LOG.info("Creating topic's title");
        actions.clearFieldForMacOS("xpath.createTopicTitleField");
        actions.clearField("xpath.createTopicTitleField");
        actions.inputNewTopicTitle(title);

        //Creating comment
        Utils.LOG.info("Filling in topic's first comment");
        actions.inputNewTopicMainText(comment);
        actions.clickCreateNewTopicButton();

        //Assert topic is created
        actions.returnToHomePage();
        String createdTopicTitle = title;
        String actualTopicTitle = actions.checkNewlyCreatedTopicTitleExist(title);
        Assert.assertEquals(createdTopicTitle,actualTopicTitle);
        Utils.LOG.info("Topic is created");

    }



    }
