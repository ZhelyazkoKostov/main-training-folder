package testCases;

import org.junit.Test;

public class TC006_CreateATopicWithoutTitleAndText extends BaseTest {

    @Test
    public void createATopicWithoutTitleAndText() {
        actions.logIn();
        actions.signIn();

        actions.clickNewTopicButton();
        actions.clickCreateNewTopicButton();

        actions.assertElementPresent("xpath.TitleRequiredWarningMsg");
        actions.assertElementPresent("xpath.postCantBeEmptyWarningMsg");
    }
}