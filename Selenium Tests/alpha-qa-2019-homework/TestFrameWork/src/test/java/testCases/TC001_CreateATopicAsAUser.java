package testCases;

import org.junit.Test;
import org.openqa.selenium.Keys;



public class TC001_CreateATopicAsAUser extends BaseTest {

    @Test
    public void createTopic() {
        String topic = actions.createUniqueTopic();

        actions.logIn();
        actions.signIn();
        actions.clickElement("xpath.createTopicButton"); // Click "New Topic" button
        actions.inputNewTopicTitle(topic);
        actions.inputNewTopicMainText(topic);
        actions.clickElement("xpath.replyAreaCreateTopicButton");
        actions.waitForElementVisible("//*[@id=\"topic-title\"]/div/div/h1/a", 10);
        actions.assertElementPresent("//*[@id=\"topic-title\"]/div/div/h1/a");

    }

}




