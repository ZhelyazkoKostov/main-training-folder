package testCases;

import org.junit.Test;

import static com.telerikacademy.testframework.Utils.getConfigProperties;
import static com.telerikacademy.testframework.Utils.getConfigPropertyByKey;

public class TC009_CreateNewTopicWith4CharsInTitleField extends BaseTest {

    @Test
    public void createNewTopicWith4CharsInTitleField() {
        actions.logIn();
        actions.signIn();
        actions.clickNewTopicButton();
        actions.inputNewTopicTitle(getConfigPropertyByKey("topicTitle4charsInput"));
        actions.inputNewTopicMainText(getConfigPropertyByKey("textbox11charsInput"));
        actions.clickNewTopicButtonInPopup();
        actions.assertElementPresent("xpath.topicTitleLengthOutOfBoundaryAlert");
    }
}
