package testCases;

import org.junit.Before;
import org.junit.Test;

public class TC063_CreateValidCommentAndCancel extends BaseTest{

    String someText = "This is a text for the textarea to be used";

    @Before
    public void logInSet(){
        actions.logIn();
        actions.signIn();

    }
    @Test
    public void verifyCancelReplyCommentFunctional(){

        actions.waitForElementVisible("xpath.searchFirstTopicFromMainList",10);
        actions.clickElement("xpath.searchFirstTopicFromMainList");
        actions.waitForElementVisible("xpath.replyOnCommentButton",10);
        actions.clickElement("xpath.replyOnCommentButton");
        actions.waitForElementVisible("xpath.createTopicTextArea",10);
        actions.clickElement("xpath.createTopicTextArea");
        actions.typeValueInField(someText,"xpath.createTopicTextArea");
        actions.clickElement("xpath.cancelReplyOnCommentButton");
        actions.waitForElementVisible("xpath.newTopicAbandonButton",10);
        actions.clickElement("xpath.newTopicAbandonButton");
        actions.assertElementNotPresent("xpath.newTopicAbandonButton");

    }


}
