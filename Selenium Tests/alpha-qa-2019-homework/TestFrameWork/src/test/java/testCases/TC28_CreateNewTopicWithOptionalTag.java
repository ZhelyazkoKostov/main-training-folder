package testCases;
import com.telerikacademy.testframework.UserActions;
import org.junit.Test;


    public class TC28_CreateNewTopicWithOptionalTag extends BaseTest {

        @Test
        public void openForumAndClickOnLoginButton(){
            UserActions actions = new UserActions();
            actions.logIn();
            actions.signIn();

            actions.clickNewTopicButton();

            // Enter topic title.
            String titleTopic = actions.createUniqueTopic();
            actions.inputNewTopicTitle(titleTopic);

            //input text in topic's body
            String bodyText = actions.getRandomString(35);
            actions.inputNewTopicMainText(bodyText);

            //push button create topic and postTopic
            actions.clickCreateNewTopicFinalButton();
        }

    }
