package testCases;

import org.junit.Test;

public class TC005_CreateANewTopicWithCategory extends BaseTest {

    @Test
    public void createNewTopicWithCategory() {
        String topicTitle = actions.getRandomString(10);
        String topicMainText = actions.getRandomString(30);

        actions.logIn();
        actions.signIn();

        actions.clickNewTopicButton();
        actions.inputNewTopicTitle(topicTitle);
        actions.inputNewTopicMainText(topicMainText);
        actions.clickElement("xpath.categoryDropDown");
        actions.clickElement("xpath.SiteFeedbackCategory");
        actions.clickCreateNewTopicButton();
    }
}
