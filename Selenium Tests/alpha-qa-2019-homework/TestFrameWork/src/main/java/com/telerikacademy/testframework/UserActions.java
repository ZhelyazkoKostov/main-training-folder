package com.telerikacademy.testframework;

import org.junit.Assert;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class UserActions {
	final WebDriver driver;

	public UserActions() {
		this.driver = com.telerikacademy.testframework.Utils.getWebDriver();
	}

	public static void loadBrowser() {
		com.telerikacademy.testframework.Utils.getWebDriver().get(Utils.getConfigPropertyByKey("base.url"));
	}

	public static void quitDriver(){
		Utils.tearDownWebDriver();
	}

	public void clickElement(String key){
		Utils.LOG.info("Clicking on element " + key);
		waitForElementVisible(key, 10);
		WebElement element = driver.findElement(By.xpath(Utils.getUIMappingByKey(key)));
		element.click();
	}

	public void typeValueInField(String value, String field){
		Utils.LOG.info("Typing: " + value + " in " + field);
		waitForElementVisible(field, 10);
		clearField(field);
		WebElement element = driver.findElement(By.xpath(Utils.getUIMappingByKey(field)));
		element.sendKeys(value);
	}

	//Add method to clear field before typing in it
    //Method is implemented in typeValueInField(String value, String field)
	public void clearField(String field){
		WebElement element = driver.findElement(By.xpath(Utils.getUIMappingByKey(field)));
		element.sendKeys(Keys.CONTROL+"a");
		element.sendKeys(Keys.BACK_SPACE);
	}

	public void clearFieldForMacOS(String field){
		WebElement element = driver.findElement(By.xpath(Utils.getUIMappingByKey(field)));
		element.sendKeys(Keys.COMMAND+"a");
		element.sendKeys(Keys.BACK_SPACE);
	}

	public String getText(String locator){
		WebElement element = driver.findElement(By.xpath(Utils.getUIMappingByKey(locator)));
		return element.getText();
	}

	//############# WAITS #########

	public void waitForElementVisible(String locator, int seconds){
		WebDriverWait wait= new WebDriverWait(driver,seconds);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(Utils.getUIMappingByKey(locator))));
	}

	public void waitForElementClickable(String locator, int seconds) {
		WebDriverWait wait= new WebDriverWait(driver,seconds);
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(Utils.getUIMappingByKey(locator))));
	}


	//############# ASSERTS #########

	public void assertElementPresent(String locator){
		Utils.LOG.info("Xpath exists: " + locator);
		Assert.assertNotNull(driver.findElement(By.xpath(Utils.getUIMappingByKey(locator))));
	}

	public void assertElementNotPresent(String locator) {
		Utils.LOG.info("Xpath does NOT exist: " + locator);
		Assert.assertEquals(0, driver.findElements(By.xpath(Utils.getUIMappingByKey(locator))).size());
	}

	public void assertTextEquals(String expectedText, String locator){
		WebDriverWait wait= new WebDriverWait(driver,10);
		String textOfWebElement = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(Utils.getUIMappingByKey(locator)))).getText();
		System.out.println(textOfWebElement);
        Utils.LOG.info("Expected text: " + expectedText + "; Actual text: " + textOfWebElement);
		Assert.assertEquals(expectedText, textOfWebElement);
	}

    public void assertTextEquals(String expectedText, String locator, String attribute) {
        WebDriverWait wait = new WebDriverWait(driver, 10);
        WebElement webElement = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(Utils.getUIMappingByKey(locator))));
        String textOfWebElement = webElement.getAttribute(attribute);
        Utils.LOG.info("Expected text in " + locator + ": " + expectedText + "; Actual text in " + locator + ": " + textOfWebElement);
        Assert.assertEquals(expectedText, textOfWebElement);
    }
	//Violeta:
	public void assertElementAttributeValueEquals(String expectedInputValue, String elementLocator, String attributeName) {
		WebDriverWait wait= new WebDriverWait(driver,10);
		WebElement  webElement = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(Utils.getUIMappingByKey(elementLocator))));
		String attributeValue = webElement.getAttribute(attributeName);
		Assert.assertEquals(expectedInputValue, attributeValue);
	}

	//############# LOGIN #########

	public void logIn(){
		waitForElementVisible("xpath.logInForumButton",10);
		clickElement("xpath.logInForumButton");
	}

	public void signIn(){
		waitForElementVisible("xpath.signInNameField",10);
		typeValueInField(Utils.getConfigPropertyByKey("username"), "xpath.signInNameField");
		typeValueInField(Utils.getConfigPropertyByKey("password"), "xpath.passwordInputField");
		clickElement("xpath.signInFormButton");
	}

	public void returnToHomePage(){
		waitForElementVisible("xpath.homepageSchoolTelerikAcademyLink",10);
		Actions action = new Actions(driver);
		action.moveToElement(driver.findElement(By.xpath(Utils.getUIMappingByKey("xpath.homepageSchoolTelerikAcademyLink"))))
				.click()
				.build()
				.perform();
	}

	public void logOut(){
		waitForElementVisible("xpath.currentUserIcon",10);
		clickElement("xpath.currentUserIcon");
		waitForElementVisible("xpath.logOutButton",10);
		clickElement("xpath.logOutButton");
	}


	//############# TOPICS #########


	public void clickNewTopicButton(){
		waitForElementVisible("xpath.createTopicButton",10);
		clickElement("xpath.createTopicButton");
	}

	public void clickNewTopicButtonInPopup() {
		waitForElementVisible("xpath.replyAreaCreateTopicButton",10);
		clickElement("xpath.replyAreaCreateTopicButton");
	}

	public void inputNewTopicTitle(String title){
		Utils.LOG.info("Typing topic title: " + title);
		waitForElementVisible("xpath.createTopicTitleField",10);
		clearField("xpath.createTopicTitleField");
		typeValueInField(title,"xpath.createTopicTitleField");
	}

	public void inputNewTopicMainText(String text){
		Utils.LOG.info("Typing comment: " + text);
		waitForElementVisible("xpath.createTopicTextArea",10);
		clearField("xpath.createTopicTextArea");
		typeValueInField(text,"xpath.createTopicTextArea");
	}

	public String createUniqueTopic(){
		Date date = new Date();
		String uniqueTopic = "Test " + date.toString();
		return uniqueTopic;
	}

	public void clickCreateNewTopicButton(){
		clickElement("xpath.replyAreaCreateTopicButton");
	}

	public String checkNewlyCreatedTopicTitleExist(String topicTitle){
		returnToHomePage();
		waitForElementVisible("//table[@class=\"topic-list ember-view\"]//child::a[contains(text(),\""+topicTitle+"\")]",10);
		WebElement createdTopicTitle = driver.findElement(By.xpath("//table[@class=\"topic-list ember-view\"]//child::a[contains(text(),\"" + topicTitle + "\")]"));
		String result = createdTopicTitle.getText();
		return result;

	}

	public void cancelTopicOrComment(){
		waitForElementVisible("xpath.replyControlAreaCancelLink",10);
		clickElement("xpath.replyControlAreaCancelLink");
	}

	public void abandonTopicFrame(){
		waitForElementVisible("xpath.newTopicAbandonConfirmationMsg",10);
		clickElement("xpath.newTopicAbandonButton");
	}

	public void keenOnTopicFrame(){
		waitForElementVisible("xpath.newTopicAbandonConfirmationMsg",10);
		clickElement("xpath.newTopicKeepButton");
	}

	public void deleteTopic() {
		//this message appears in the text area in place of our text after deleting a topic
		String expectedTextAfterDeleting = "(topic withdrawn by author, will be automatically deleted in 24 hours unless flagged)";

		waitForElementVisible("xpath.threeDotsTopicArea", 10);

		clickElement("xpath.threeDotsTopicArea"); //click on the three dots
		clickElement("xpath.deleteTopicButton"); //we see the button for deletion and we click it

		waitForElementVisible("xpath.deletedTopicConfirmationMsg", 10); //we wait for the message after deletion

		assertTextEquals(expectedTextAfterDeleting,"xpath.deletedTopicConfirmationMsg");

	}

	public void deleteTopicZK() {
		//this message appears in the text area in place of our text after deleting a topic
		String expectedTextAfterDeleting = "(topic withdrawn by author, will be automatically deleted in 24 hours unless flagged)";

		waitForElementVisible("xpath.threeDotsTopicArea", 10);

		clickElement("xpath.threeDotsTopicArea"); //click on the three dots
		clickElement("xpath.deleteTopicButton"); //we see the button for deletion and we click it

		waitForElementVisible("xpath.reverseDeletedComment", 10); //we wait for THE REVERSE BUTTON IS PRESENT

		assertElementPresent("xpath.reverseDeletedComment");
	}

	public void clickCreateNewTopicFinalButton(){
		waitForElementVisible("xpath.createNewTopicFinalButton", 10);
		clickElement("xpath.createNewTopicFinalButton");
	}

	public String getRandomString(int n) {

		// chose a Character random from this String
		String AlphaNumericString = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
				+ "0123456789"
				+ "abcdefghijklmnopqrstuvxyz";

		// create StringBuffer size of AlphaNumericString
		StringBuilder sb = new StringBuilder(n);

		for (int i = 0; i < n; i++) {

			// generate a random number between
			// 0 to AlphaNumericString variable length
			int index
					= (int)(AlphaNumericString.length()
					* Math.random());

			// add Character one by one in end of sb
			sb.append(AlphaNumericString
					.charAt(index));
		}

		return sb.toString();
	}

    // Editing a topic

    public String checkEditedTopicTitleExist(String topicTitle) {
        returnToHomePage();
        waitForElementVisible("//table[@class=\"topic-list ember-view\"]//child::a[contains(text(),\"" + topicTitle + "\")]", 10);
        WebElement editedTopicTitle = driver.findElement(By.xpath("//table[@class=\"topic-list ember-view\"]//child::a[contains(text(),\"" + topicTitle + "\")]"));
        String result = editedTopicTitle.getText();
        return result;

    }

    public void selectCategory() {
        clickElement("xpath.editCategoryDropdown");
        clickElement("xpath.siteFeedbackCategoryOption");

    }

    /*public void createLabel() {
        String tag = getRandomString(5);
        clickElement("xpath.editTagsDropdown");
        clickElement("xpath.createTagInEditTagsDropdown");
        typeValueInField(tag, "xpath.createTagInEditTagsDropdown");
        waitForElementVisible("xpath.createTagButton", 5);
        clickElement("xpath.createTagButton");
    }*/


    /*public boolean specificCategoryNotAllowingRandomLabelsError() {
        boolean error = true;
//		List<WebElement> elementPopupError = driver.findElements(By.xpath(Utils.getUIMappingByKey("xpath.specificCategoryNotAllowingRandomLabelsError")));
//			List<WebElement> elementPopupError = driver.findElements(By.className("btn btn-primary"));
        WebElement body = driver.findElement(By.tagName("body"));
        Utils.LOG.info(body.getAttribute("class"));
//        if (driver.findElements(By.xpath(Utils.getUIMappingByKey("xpath.specificCategoryNotAllowingRandomLabelsError"))).isEmpty()) { //.isEmpty()
        if(driver.findElements(By.xpath(Utils.getUIMappingByKey("xpath.specificCategoryNotAllowingRandomLabelsError"))).size() > 0) {
//			if (elementPopupError.size() !=0){
            clickElement("xpath.specificCategoryNotAllowingRandomLabelsError");
            clickElement("xpath.specificCategoryNotAllowingRandomLabelsOKButton");

        } else {
            error = false;
        }
        return error;
    }

    public void driverWait() {
        driver.manage().timeouts().implicitlyWait(2000, TimeUnit.SECONDS);

    }*/

	public void typeHugeText (String text, String locator) {

		Utils.LOG.info("Typing comment: " + text);
		waitForElementVisible(locator,10);
		clearField(locator);

		WebElement element = driver.findElement(By.xpath(Utils.getUIMappingByKey(locator)));
		((JavascriptExecutor)driver).executeScript("arguments[0].value = arguments[1];", element,
				text);

	}

	public void selectAllTextInTextArea() {
		waitForElementVisible("xpath.createTopicTextArea", 10);
		WebElement selectAll = driver.findElement(By.xpath(Utils.getUIMappingByKey("xpath.createTopicTextArea")));
		selectAll.sendKeys(Keys.chord(Keys.CONTROL, "a"));
	}

	public void applyBoldToTextInTextArea() {
		waitForElementClickable("xpath.boldTextButton", 10);
		clickElement("xpath.boldTextButton");
	}

	public void applyItalicToTextInTextArea() {
		waitForElementClickable("xpath.italicTextButton", 10);
		clickElement("xpath.italicTextButton");
	}


    public void applyBlockquoteToTextInTextArea() {
        waitForElementClickable("xpath.blockquoteTextButton", 10);
        clickElement("xpath.blockquoteTextButton");
    }

    public void applyPreformattedTextToTextInTextArea() {
		waitForElementClickable("xpath.preformattedTextButton", 10);
		clickElement("xpath.preformattedTextButton");
	}

	public void applyBulletedListToTextInTextArea() {
		waitForElementClickable("xpath.bulletedListTextButton", 10);
		clickElement("xpath.bulletedListTextButton");
	}

	public void applyNumberedListToTextInTextArea() {
		waitForElementClickable("xpath.numberedListTextButton", 10);
		clickElement("xpath.numberedListTextButton");
	}

	public String getTextOfElement(String locator){
		String text = driver.findElement(By.xpath(Utils.getUIMappingByKey(locator))).getText();
		return text;
	}
}