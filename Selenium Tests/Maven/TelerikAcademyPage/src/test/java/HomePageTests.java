import Base.BaseTest;

import Pages.*;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class HomePageTests extends BaseTest {

    HomePage homePage;
    AlphaPage alphaPage;
    MasterPage masterPage;
    SchoolPage schoolPage;
    ForBusinessPage forBusinessPage;
    AboutPage aboutPage;
    LogInPage logInPage;

    public HomePageTests(){
        super();
    }

    @BeforeMethod
    public void preSet(){
        browserInitialization();
        homePage = new HomePage();

    }

    @Test
    public void checkTitle(){

        String pageTitle = driver.getTitle();
        Assert.assertEquals(pageTitle, "Programming and Digital Training - Telerik Academy");
    }

    @Test
    public void verifyAlphaButtonClickedTest(){
        alphaPage = homePage.alphaLinkButtonClick();

    }

    @Test
    public void verifyMasterLinkButtonClickedTest(){
        masterPage = homePage.masterLinkButtonClick();

    }

    @Test
    public void schoolLinkButtonClickedTest(){
        schoolPage = homePage.schoolLinkButtonClick();
    }

    @Test
    public void forBusinessLinkButtonClickedTest(){
        forBusinessPage = homePage.forBusinessLinkButtonClick();
    }

    @Test
    public void aboutLinkButtonClickedTest(){
        aboutPage = homePage.aboutLinkButtonClick();
    }

    @Test
    public void logInButtonCheckedTest(){
        logInPage = homePage.logInPageButtonClick();
    }

    @Test
    public void imgLogoDisplayTest() {

        Assert.assertTrue(homePage.checkImageLogo());
    }

    @Test
    public void scrollToEmailContactTest()throws Exception{
        homePage.scrollToEmailContact();
        Thread.sleep(10000);

    }

    @AfterMethod
    public void testClosure(){
        driver.close();
    }








}
