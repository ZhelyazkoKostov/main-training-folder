import Base.BaseTest;
import Pages.HomePage;
import Pages.LogInPage;
import org.testng.Assert;
import org.testng.ReporterConfig;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class LogInPageTest extends BaseTest {

    HomePage homePage;
    LogInPage logInPage;


    public LogInPageTest(){
        super();
    }

    @BeforeMethod
    public void preSet(){
        browserInitialization();
        homePage = new HomePage();
        logInPage = homePage.logInPageButtonClick();
    }

    @Test
    public void verifyLogInWithCorrectUserAndPassword(){

        logInPage.logIn();
    }

    @Test
    public void verifyLogInWithMissingUserName(){
        Assert.assertTrue(logInPage.logInWithoutUsername());
    }

    @Test
    public void verifyLogInWithMissingPassword(){
        Assert.assertTrue(logInPage.logInWithoutPassword());
    }

    @Test
    public void verifyLogInWithInvalidEmail(){
        Assert.assertFalse(logInPage.logInWithInvalidEmail());
    }

    @AfterMethod
    public void testClosure(){
        driver.quit();
    }



}
