package Base;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import static Utils.TestUtil.IMPLICIT_WAIT;
import static Utils.TestUtil.PAGE_LOAD_TIMEOUT;

public class BaseTest {

    public static WebDriver driver;
    public static Properties prop;

    public BaseTest(){

        try {
            prop = new Properties();
            FileInputStream ip = new FileInputStream(
                    "C:/Users/Administrator/IdeaProjects/main-training-folder/Selenium Tests/Maven/TelerikAcademyPage/src/main/java/Config/config.properties");
            prop.load(ip);
        } catch (FileNotFoundException e) {
            System.out.println(e.getMessage());
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }

    }


    public static void browserInitialization(){

        String browser = prop.getProperty("browser");
        if(browser.equals("chrome")) {
            System.setProperty("webdriver.chrome.driver", "C:\\Users\\Administrator\\IdeaProjects\\main-training-folder\\Selenium Tests\\Maven\\TelerikAcademyPage\\chromedriver\\chromedriver.exe");
            driver = new ChromeDriver();
        }
        else if(browser.equals("firefox")){
            System.setProperty("webdriver.gecko.driver", "C:\\Users\\Administrator\\IdeaProjects\\main-training-folder\\Selenium Tests\\Maven\\TelerikAcademyPage\\geckodriver\\geckodriver.exe");
            driver = new FirefoxDriver();
        }

        driver.manage().window().maximize();
        driver.manage().deleteAllCookies();
        driver.manage().timeouts().implicitlyWait(IMPLICIT_WAIT, TimeUnit.SECONDS);
        driver.manage().timeouts().pageLoadTimeout(PAGE_LOAD_TIMEOUT, TimeUnit.SECONDS);

        driver.get(prop.getProperty("url"));
    }
}
