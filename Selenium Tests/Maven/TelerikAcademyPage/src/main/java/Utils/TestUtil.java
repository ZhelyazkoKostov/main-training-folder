package Utils;

public class TestUtil {

    public final static long PAGE_LOAD_TIMEOUT = 20;
    public final static long IMPLICIT_WAIT = 10;
    public final static int EXPLICIT_WAIT_FOR_LOGIN_FIELDS_TO_LOAD = 10;
}
