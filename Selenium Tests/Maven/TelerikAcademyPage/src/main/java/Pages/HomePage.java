package Pages;

import Base.BaseTest;
import org.openqa.selenium.*;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class HomePage extends BaseTest {


    @FindBy(xpath = "/html/body/div[2]/div/div/a/img")
    WebElement logoImage;

    @FindBy(xpath = "/html/body/div[2]/div/div/div[3]/a[1]")
    WebElement alphaLinkButton;

    @FindBy(xpath = "/html/body/div[2]/div/div/div[3]/a[2]")
    WebElement masterLinkButton;

    @FindBy(xpath = "/html/body/div[2]/div/div/div[3]/a[2]")
    WebElement schoolLinkButton;

    @FindBy(xpath = "/html/body/div[2]/div/div/div[3]/a[2]")
    WebElement forBuniessLinkButton;


    @FindBy(xpath = "/html/body/div[2]/div/div/div[3]/a[2]")
    WebElement AboutLinkButton;

    @FindBy(xpath = "/html/body/div[2]/div/div/div[3]/a[6]")
    WebElement logInButton;

    @FindBy(linkText = "info@telerikacademy.com")
    WebElement emailContact;


    public HomePage(){

        PageFactory.initElements(driver, this);

    }

    public boolean checkImageLogo(){

        return logoImage.isDisplayed();
    }

    public AlphaPage alphaLinkButtonClick(){

        alphaLinkButton.click();
        return new AlphaPage();
    }

    public MasterPage masterLinkButtonClick(){

        masterLinkButton.click();
        return new MasterPage();
    }

    public SchoolPage schoolLinkButtonClick(){

        schoolLinkButton.click();
        return new SchoolPage();
    }

    public ForBusinessPage forBusinessLinkButtonClick(){

        forBuniessLinkButton.click();
        return new ForBusinessPage();
    }

    public AboutPage aboutLinkButtonClick(){

        alphaLinkButton.click();
        return new AboutPage();
    }

    public LogInPage logInPageButtonClick(){

        logInButton.click();
        return new LogInPage();
    }

    public void scrollToEmailContact(){
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("arguments[0].scrollIntoView();", emailContact);
        emailContact.click();
    }



}
