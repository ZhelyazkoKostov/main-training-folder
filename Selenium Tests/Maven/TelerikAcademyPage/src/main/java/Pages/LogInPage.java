package Pages;

import Base.BaseTest;
import Utils.TestUtil;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import static Utils.TestUtil.EXPLICIT_WAIT_FOR_LOGIN_FIELDS_TO_LOAD;

public class LogInPage extends BaseTest {

    HomePage homePage;
    WebDriverWait wait = new WebDriverWait(driver, EXPLICIT_WAIT_FOR_LOGIN_FIELDS_TO_LOAD);

    @FindBy(xpath="//*[@id=\"signInName\"]")
    WebElement username;

    @FindBy(id="password")
    WebElement pwd;

    @FindBy(id="next")
    WebElement signInButton;

    public LogInPage(){
        PageFactory.initElements(driver, this);
    }

    public String switchToWindow(){
        Set<String> winTitle = driver.getWindowHandles();
        List<String> list = new ArrayList<String>(winTitle);
        String window = list.get(1);
        return window;
    }

    public MainPageAfterLogIn logIn(){
        driver.switchTo().window(switchToWindow());

//        try{
//            Thread.sleep(10000);
//        } catch (InterruptedException e) {
//            System.out.println(e.getMessage());
//        }


       WebElement inputMail = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"signInName\"]")));
       inputMail.sendKeys(prop.getProperty("emailLogIn"));
       pwd.sendKeys(prop.getProperty("passwordLogIn"));
       signInButton.click();

       return new MainPageAfterLogIn();
    }

    public boolean logInWithoutUsername(){
        driver.switchTo().window(switchToWindow());
        WebElement inputPass = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("password")));
        inputPass.sendKeys(prop.getProperty("passwordLogIn"));
        signInButton.click();
        WebElement missingEmail = driver.findElement(By.xpath("//*[@id=\"api\"]/div/div[3]/div[1]/div/p"));
        return missingEmail.isDisplayed();
    }

    public boolean logInWithoutPassword(){
        driver.switchTo().window(switchToWindow());
        WebElement inputMail = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("signInName")));
        inputMail.sendKeys(prop.getProperty("emailLogIn"));
        signInButton.click();
        WebElement missingPassword = driver.findElement(By.xpath("//*[@id=\"api\"]/div/div[3]/div[2]/div[2]/p"));
        return missingPassword.isDisplayed();
    }

    public boolean logInWithInvalidEmail(){
        driver.switchTo().window(switchToWindow());
        WebElement inputMail = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("signInName")));
        inputMail.sendKeys(prop.getProperty("wrongEmail"));
        signInButton.click();
        WebElement wrongEmail = driver.findElement(By.xpath("//*[@id=\"api\"]/div/div[3]/div[1]/div/p"));
        return wrongEmail.isDisplayed();
    }


}
