const getGets = (arr) => {
    let index = 0;
  
    return () => {
      const toReturn = arr[index];
      index += 1;
      return toReturn;
    };
  };
  
  // This is the place where you must place your test data
  const test = [
    '5', 
    '-2',
    '-1',
    '-10',
    '-6',
    '3'
    
  ];
  
  const gets = this.gets || getGets(test);
  const print = this.print || console.log;

  const number = Number(gets());

  let maxValue = +gets();

  for(let i = 0; i < arrSize-1; i++){
      let num = +gets();
      if(maxValue < num){
          maxValue = num;
      }
  }

  print(maxValue);