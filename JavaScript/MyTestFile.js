const getGets = (arr) => {
  let index = 0;

  return () => {
    const toReturn = arr[index];
    index += 1;
    return toReturn;
  };
};

// This is the place where you must place your test data
const test = [
  '3', // This is the first line from the test.
  '2' // This is the second line from the test.
];

const gets = this.gets || getGets(test);
const print = this.print || console.log;

const a = (+gets());
const b = Number(gets());

print(a + b);
