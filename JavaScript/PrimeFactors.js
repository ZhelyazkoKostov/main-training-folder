const getGets = (arr) => {
    let index = 0;
  
    return () => {
      const toReturn = arr[index];
      index += 1;
      return toReturn;
    };
  };
  
  // This is the place where you must place your test data
  const test = [
    '113'
    
  ];
  
  const gets = this.gets || getGets(test);
  const print = this.print || console.log;



  let number = Number(gets());
  let count = 2;
  while (number > 1){
      if(number % count === 0){
          number = number / count;
          print(count);
      }
      else {
          count++;
      }
  }