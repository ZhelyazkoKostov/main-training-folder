const getGets = (arr) => {
    let index = 0;
  
    return () => {
      const toReturn = arr[index];
      index += 1;
      return toReturn;
    };
  };
  
  // This is the place where you must place your test data
  const test = [
    '5', 
    '2',
    '1',
    '1',
    '6',
    '3'
    
  ];
  
  const gets = this.gets || getGets(test);
  const print = this.print || console.log;

  const arrSize = Number(gets());
  let arr = [];
  let odd = 1;
  let even = 1;

  for(let i = 0; i < arrSize; i++){
    arr.push(+gets());
  }

  for(let i = 0; i < arrSize; i+=2){
      odd = odd * arr[i];
  }

  for(let i = 1; i < arrSize; i+=2){
    even = even * arr[i];
}

  

  if(odd === even){
      print("yes " + odd);
  }
  else {
      print ("no "+ odd + " " + even);
  }
