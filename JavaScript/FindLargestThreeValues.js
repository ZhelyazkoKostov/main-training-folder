const getGets = (arr) => {
    let index = 0;
  
    return () => {
      const toReturn = arr[index];
      index += 1;
      return toReturn;
    };
  };
  
  // This is the place where you must place your test data
  const test = [
    '6', 
    '9',
    '11',
    '3',
    '2',
    '1',
    '8'
  ];
  
  const gets = this.gets || getGets(test);
  const print = this.print || console.log;
  
  const a = Number(gets());
  let fin;
  let arr=[];
  for(let c =0;c < a; c++){
    arr.push(+gets());
    
  }

  arr.sort(function(a, b){return a-b});
  fin=arr.pop()+ ", ";
  fin=fin + arr.pop()+ " and ";
  fin=fin + arr.pop();

  print(fin);

  