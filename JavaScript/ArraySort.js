const getGets = (arr) => {
    let index = 0;
  
    return () => {
      const toReturn = arr[index];
      index += 1;
      return toReturn;
    };
  };
  
  // This is the place where you must place your test data
  const test = [
    '0,1,0,3,12'
];
  
  const gets = this.gets || getGets(test);
  const print = this.print || console.log;

  let line = gets().split(",");
  let count = 0;

  for(let i = 0; i < line.length; i++){
      if(line[count]=='0'){
          line.splice(count,1);
          line.push(0);

      }
      else {count++;}
  }
  
  
  let result ="";
  for(let i = 0; i<line.length-1; i++){
    result = result + line[i] + ","
  }

result = result + line[line.length-1];
print(result);