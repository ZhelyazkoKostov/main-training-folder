const getGets = (arr) => {
    let index = 0;
  
    return () => {
      const toReturn = arr[index];
      index += 1;
      return toReturn;
    };
  };
  
  // This is the place where you must place your test data
  const test = [
    'c,c,a,b,a,a,b,c'
];
  
  const gets = this.gets || getGets(test);
  const print = this.print || console.log;

  let count = 0;
  let line = gets().split(",");
  for(let i = 0; i < line.length+1; i++){
    for (let k = i+1; k <line.length; k++){
      if(line[i]=== line[k]){
        line.splice(k,1);
        k--;
      }
  
    }

  }

  let res = "";
  for(let i = 0; i < line.length-1; i++) {
    res = res + line[i] + ",";
  }
  res = res + line[line.length-1];
  print(res);



  
