const getGets = (arr) => {
    let index = 0;
  
    return () => {
      const toReturn = arr[index];
      index += 1;
      return toReturn;
    };
  };
  
  // This is the place where you must place your test data
  const test = [
    '2',
    '1,2,3,4,5',
    '1,2,8,9,9'
];
  
  const gets = this.gets || getGets(test);
  const print = this.print || console.log;

  let numRows = +gets();
  let checker = 1;

  for(let i = 0; i < numRows; i++){
    let arr = [];
    let str = gets().split(",");
    
    for (let k = 1; k < str.length + 1; k++){
     if(+(str[k-1])<= +(str[k])){
      checker ++;
     }
    }
    if(checker == str.length){
      print(true);
    }
    else {
      print(false);
    }
    checker = 1;
  
  }

  
  