const getGets = (arr) => {
    let index = 0;
  
    return () => {
      const toReturn = arr[index];
      index += 1;
      return toReturn;
    };
  };
  
  // This is the place where you must place your test data
  const test = [
    '1, 2, 3'
];
  
  const gets = this.gets || getGets(test);
  const print = this.print || console.log;

  let line = gets().split(", ");
  let max = 0;
  let newArr = [];
  let count = line.length;
  let indexed = 0;

  for (let k = 0; k < count; k++){
    for (let i = 0; i < line.length; i++){
        if(max < +line[i]){
            max = line[i];
            indexed = i;
        }
    }
    newArr[k] = max;
    line.splice(indexed,1);
    max = 0;
}

let result ="";
  for(let i = 0; i<newArr.length-1; i++){
    result = result + newArr[i] + ", "
  }

  result = result + newArr[newArr.length-1];
  print(result);
  
