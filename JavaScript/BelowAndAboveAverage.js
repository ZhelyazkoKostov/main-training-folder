const getGets = (arr) => {
    let index = 0;
  
    return () => {
      const toReturn = arr[index];
      index += 1;
      return toReturn;
    };
  };
  
  // This is the place where you must place your test data
  const test = [
    '3,-12,0,0,13,5,1,0,-2'
];
  
  const gets = this.gets || getGets(test);
  const print = this.print || console.log;

  let sum = 0;
  let line = gets().split(",");
  for (let i=0;i<line.length;i++){
    sum = sum + +line[i];
  }

  let ave = Number(sum / line.length);
  let below = [];
  let above = [];
  let countA = 1;
  let countB = 1;
  let resA = "above: ";
  let resB = "below: ";

  for(let i = 0; i < line.length; i++){
      if(+line[i]<ave) {

          below[countB] = +line[i]; 
          countB++;
      }
      else if(+line[i]>ave){
          above[countA] = +line[i];
          countA++;
      }
  }
  print("avg: " + ave.toFixed(2));

  for (let i = 1; i < below.length-1; i++){
    resB = resB + below[i] + ",";
  }
  resB = resB + below[below.length-1];
  print(resB);

  for (let i = 1; i < above.length-1; i++){
    resA = resA + above[i] + ",";
  }
  resA = resA + above[above.length-1];
  print(resA);
