const getGets = (arr) => {
    let index = 0;
  
    return () => {
      const toReturn = arr[index];
      index += 1;
      return toReturn;
    };
  };
  
  // This is the place where you must place your test data
  const test = [
    '6,0,4,4',
    '4,0,4,3'

    
  ];
  
  const gets = this.gets || getGets(test);
  const print = this.print || console.log;

 
  let arr1 = [];
  let arr2 = [];
  let resArr = [];
  arr1 = gets().split(",");
  arr2 = gets().split(",");
  
  for(let i = 0; i < arr1.length; i++){

    resArr.push(arr1[i]);
    resArr.push(arr2[i]);
    
  }
  print(resArr);
  
 
