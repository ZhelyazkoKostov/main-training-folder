const getGets = (arr) => {
    let index = 0;
  
    return () => {
      const toReturn = arr[index];
      index += 1;
      return toReturn;
    };
  };
  
  // This is the place where you must place your test data
  const test = [
    '2,1,3,4',
    '5'
];
  
  const gets = this.gets || getGets(test);
  const print = this.print || console.log;

  let line = gets().split(",");
  let rotateNum = +gets();
  let count = 0;

  for(let i = 0; i < rotateNum; i++){
      let k = line[count];
      line.push(k);
      line.shift(line[count]);
      
  }
  let result ="";
  for(let i = 0; i<line.length-1; i++){
    result = result + line[i] + ","
  }

  result = result + line[line.length-1];
  print(result);