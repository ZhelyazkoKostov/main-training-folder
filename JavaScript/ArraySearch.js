const getGets = (arr) => {
    let index = 0;
  
    return () => {
      const toReturn = arr[index];
      index += 1;
      return toReturn;
    };
  };
  
  // This is the place where you must place your test data
  const test = [
    '0,2,0,3'
];
  
  const gets = this.gets || getGets(test);
  const print = this.print || console.log;

  let line = gets().split(",");
  let arr = [];
  let checker = 0;
  let count = 0;

  for(let i = 1; i < line.length + 1; i++){
    for(let k = 0; k < line.length; k++){
      if(line[k] == i){
        checker++;
      }
    }
    if(checker == 0){
      arr[count] = i;
      count++;
    }
    checker = 0;
  }

  let result ="";
  for(let i = 0; i<arr.length-1; i++){
    result = result + arr[i] + ","
  }

  result = result + arr[arr.length-1];
  print(result);


  
      

