# Social Network Test Plan

>   Version 1.0, October 23, 2019

>   Prepared by Zhelyazko Kostov and Nino Simeonov


---
### **TABLE OF CONTENTS**

[1.	INTRODUCTION](#introduction)

        1.1 OBJECTIVES

[2.	SCOPE](#scope)

        2.1 FEATURES TO BE TESTED
        2.2 FEATURES NOT TO BE TESTED

[3.	APPROACH](#approach)

[4.	TESTING PROCESS](#testing_process)

        4.1 TEST DELIVERABLES
        4.2 RESPONSIBILITIES
        4.3 RESOURCES
        4.4 ESTIMATION AND SCHEDULE

[5.	ENVIRONMENT REQUIREMENTS](#env_Requirements)



## **1. <a name="introduction"></a> Introduction**

The application under test (AUT) represents a social network (SN) concept. It will contain all main functionalities and features related to a SN as:

•	Search for people;

•	Connect to people;

•	Create, comment and like posts;

•	Get a feed of the newest/most relevant posts of your connections;

•	Change and update personal information;

•	Administrative part with functionalities like:

        -  	Edit/Delete Profile;
        -	Edit/Delete Post;
        -	Edit/Delete Comments;

### **1.1 Objectives**

__The scope of the project will include:__

•	Testing of the web services, using Postman to automate the test cases. The APIs will be  accessed from Swagger. After creating the test cases, the automation will be done within 3 days;

•	Testing of the main functionalities of the SN. Testing will include both, manual and automation testing techniques. 

    -	The automation part will be developed using Selenium. Some of the tests will be developed following BDD principles. After developing the test cases, the automation will be implemented within one week.

    -	The manual testing will cover happy paths and main functionalities, as well as non-automating features.  The manual testing will be executed within two days.

•	Database exploratory testing. The objectives are to verify correct and sensible connections between tables, correct and sensible values and constrains allocated. This testing will be done within one day.

•	Preparation of detailed tests and bugs reports. The non- automating reports will be designed and created within three days.

## **2.	<a name="scope"></a>Scope**
### **2.1 Features to be tested**
•	Creation of a profile;

•	Search, connect and disconnect from a profile;

•	Create, like, unlike and delete a post. Set visibility status. Post likes counter;

•	Create, like, unlike and delete, reply to a comment;

•	Visibility and non-visibility of a profile information (depending on profile settings);

•	Visibility of publicly set posts;

•	Visibility of public feeds;

•	Personal profile information update:

    -	Change name;
	-   Upload profile picture. Set visibility status;
	-   Update age, e-mail, nationality.

•	Generation of personal post feeds. Sort order;

•	Administration properties:

	-   Edit/delete profile;
	-   Edit/delete post;
	-   Edit/delete comment.

### **2.2 Features not to be tested**

## **3.	 <a name="approach"></a>Approach**

The AUT features are separated into four major groups:

•	Public part features;

•	Registration part;

•	Private part;

    -	Profile settings and updates;
    -	Connections;
    -	Posts;
    -	Comments;
    -	Posts feed;

•	Administrative part;

---
- Exploratory manual testing will be performed on the main features and happy paths, using end-to-end test cases;

-	The approach for testing the Registration part will be automation for all features.

Here it will be used boundary value analysis, pairwise testing. The automation will cover all positive and negative options of the registration phase;
-	The approach for testing the Public part will be to test all features manually;
-	The approach for testing the Private part will comprise both manual and automation testing;

In this section all happy paths will be covered with automation testing. Automation testing will also be used for the main functionalities for all subsections excluding the Posts feed, where manual testing techniques will be used.

Here will be performed functional testing for all main features, integration testing for the correct connection and transition of the separate elements.

It will be used boundary value analysis for posts and comments, equivalence partitioning for public and private settings, state transition testing for major functionality options.

Finally it will be used usability testing to verify ease of the application exploit.

For this section will be used some test cases developed on the BDD concept.
-	The approach for testing the Administrative part will be automation for all features;





