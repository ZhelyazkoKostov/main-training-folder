package Pages;

import BasePage.BasePages;
import BasePage.GLobalMethods;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

public class HomePage extends BasePages {


    GLobalMethods gLobalMethods = new GLobalMethods();
    String topicTitle = gLobalMethods.createUniqueTopic();
    String topicMainText = "This text " + gLobalMethods.createUniqueTopic() + " has been autogenerated";



    public void createNewTopic(){
        gLobalMethods.waitUi_ElementPresentByXpath("topic.xpath.newTopicButton",10);
        gLobalMethods.clickUi_ElementByXpath("topic.xpath.newTopicButton");
        WebElement topicInputsField = gLobalMethods.createWebElementByXpath("topic.xpath.newTopicInputFieldsArea");
        Assert.assertTrue(topicInputsField.isDisplayed());
        gLobalMethods.typeInFieldById("topic.id.titleInputFieldNewTopic", topicTitle);
        gLobalMethods.typeInFieldByXpath("topic.xpath.newTopicMainInputFieldTextArea", topicMainText);
        gLobalMethods.clickUi_ElementByXpath("topic.xpath.newTopicCreateTopicButton");
        gLobalMethods.waitUi_ElementPresentByXpath("topic.xpath.newlyCreatedTopicTitle",10);
        String newTopicTitle = gLobalMethods.getTextWebElementByXpath("topic.xpath.newlyCreatedTopicTitle");
        String expected = topicTitle;
        Assert.assertEquals(expected, newTopicTitle);




    }
}
