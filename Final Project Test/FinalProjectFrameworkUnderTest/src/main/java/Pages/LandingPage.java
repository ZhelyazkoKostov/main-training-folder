package Pages;

import BasePage.BasePages;
import BasePage.GLobalMethods;
import TestUtils.Utils;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

public class LandingPage extends BasePages {

    private GLobalMethods gLobalMethods = new GLobalMethods();
    private String username = getConfigPropertyByKey("usernameForSignIn");
    private String password = getConfigPropertyByKey("passwordForSignIn");
    private Utils utils = new Utils();


    @FindBy(xpath = "//button[@class=\"widget-button btn btn-primary btn-small login-button btn-icon-text\"]/child::span[contains(text(),\"Log In\")]")
    WebElement loginButton;

    @FindBy (xpath = "//div[@class=\"localAccount\"]")
    WebElement loginForm;

    @FindBy (id = "signInName")
    WebElement usernameField;

    @FindBy (id = "password")
    WebElement passwordField;

    @FindBy (id="next")
    WebElement signinButton;

    public LandingPage(){
        PageFactory.initElements(driver,this);
//        driver.get(utils.getLANDING_PAGE_URL());
    }



    public boolean isLandingInitialized(){
        gLobalMethods.waitWebElementPresent(loginButton,10);
        if(loginButton.isDisplayed()){
            System.out.println("Login Button displayed successfully!");
        }
        else {
            System.out.println("Login Button was not displayed!");
        }
        return loginButton.isDisplayed();
    }

    public HomePage loginWith(){
        gLobalMethods.waitWebElementPresent(loginButton,10);
        gLobalMethods.clickWebElement(loginButton);
        gLobalMethods.waitWebElementPresent(loginForm,10);
        Assert.assertTrue(loginForm.isDisplayed());
        gLobalMethods.typeInFieldByWebElement(usernameField, username);
        gLobalMethods.typeInFieldByWebElement(passwordField, password);
        gLobalMethods.clickWebElement(signinButton);
        WebElement avatarIcon = gLobalMethods.createWebElementById("landingPage.id.avatarIcon");
        gLobalMethods.waitWebElementPresent(avatarIcon,10);
        Assert.assertTrue(avatarIcon.isDisplayed(),"Verifies the login successful!");
        return new HomePage();
    }

    public void getTitle(){
        String actual = "Училищна Телерик Академия";
        String expected = driver.getTitle();
        Assert.assertEquals(expected, actual);
    }


}
