package BasePage;

import Pages.HomePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import java.util.Date;

public class GLobalMethods extends BasePages {



// -------------- WAIT METHODS --------------

    public void waitWebElementPresent(WebElement element, int seconds){
        WebDriverWait wait =  new WebDriverWait(driver, seconds);
        wait.until(ExpectedConditions.visibilityOf(element));
    }

    public void waitUi_ElementPresentByXpath(String xpathLocator, int seconds){
        WebDriverWait wait = new WebDriverWait(driver, seconds);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(getUiMappingPropertyByKey(xpathLocator))));
    }

    public void waitUi_ElementPresentById(String idLocator, int seconds){
        WebDriverWait wait = new WebDriverWait(driver, seconds);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(getUiMappingPropertyByKey(idLocator))));
    }



// -------------- CLICK METHODS --------------

    public void clickWebElement(WebElement element){
        Assert.assertTrue(element.isEnabled(), "The "+ element +" is not enabled!" );
        element.click();
        System.out.println("Element "+ element + " was clicked!");
    }

    public void clickUi_ElementByXpath(String xpath){
        WebElement element = driver.findElement(By.xpath(getUiMappingPropertyByKey(xpath)));
        Assert.assertTrue(element.isEnabled(), "The "+ element +" is not enabled!" );
        element.click();
        System.out.println("Element "+ element + " was clicked!");
    }

    public void clickUi_ElementById(String id){
        WebElement element = driver.findElement(By.id(getUiMappingPropertyByKey(id)));
        Assert.assertTrue(element.isEnabled(), "The "+ element +" is not enabled!" );
        element.click();
        System.out.println("Element "+ element + " was clicked!");
    }



// -------------- TYPE METHODS --------------

    public void clearFieldByXpath(String field){
        WebElement element = driver.findElement(By.xpath(getUiMappingPropertyByKey(field)));
        element.clear();
    }

    public void clearFieldById(String field){
        WebElement element = driver.findElement(By.id(getUiMappingPropertyByKey(field)));
        element.clear();
    }

    public void clearFieldByWebElement(WebElement element){
        element.clear();
    }

    public void typeInFieldByWebElement(WebElement element, String text) {
        Assert.assertTrue(element.isEnabled(), "The "+ element +" field is not enabled!" );
        clearFieldByWebElement(element);
        element.sendKeys(text);
    }

    public void typeInFieldByXpath(String field, String text){
        waitUi_ElementPresentByXpath(field,10);
        clearFieldByXpath(field);
        driver.findElement(By.xpath(getUiMappingPropertyByKey(field))).sendKeys(text);
    }

    public void typeInFieldById(String field, String text){
        waitUi_ElementPresentById(field,10);
        clearFieldById(field);
        driver.findElement(By.id(getUiMappingPropertyByKey(field))).sendKeys(text);
    }


// -------------- CREATE WEB ELEMENT METHODS --------------

    public WebElement createWebElementById(String id){
        waitUi_ElementPresentById(id,10);
        WebElement element = driver.findElement(By.id(getUiMappingPropertyByKey(id)));
        return element;
    }

    public WebElement createWebElementByXpath(String xpath){
        waitUi_ElementPresentByXpath(xpath,10);
        WebElement element = driver.findElement(By.xpath(getUiMappingPropertyByKey(xpath)));
        return element;
    }


// -------------- GET ATTRIBUTE OF A WEB ELEMENT --------------

    public String getTextWebElementByXpath(String xpath){
        String text = driver.findElement(By.xpath(getUiMappingPropertyByKey(xpath))).getText();
        return text;
    }



// -------------- CREATE RANDOM TOPIC TITLE --------------

    public String createUniqueTopic(){
        Date date = new Date();
        String uniqueTopic = "Test " + date.toString();
        return uniqueTopic;
    }




}
