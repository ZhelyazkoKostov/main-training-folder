package PageTests;

import BaseTest.BaseTests;
import Pages.HomePage;
import Pages.LandingPage;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class HomePageTests extends BaseTests {

    private LandingPage landingPage = new LandingPage();
    private HomePage homePage;

    @BeforeClass
    public void initiateLandingPage(){

        homePage = landingPage.loginWith();
    }

    @Test
    public void verifyNewTopicSuccessfullyCreated(){

        homePage.createNewTopic();
    }


}
