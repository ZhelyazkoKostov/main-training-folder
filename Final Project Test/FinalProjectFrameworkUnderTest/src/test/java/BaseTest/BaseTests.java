package BaseTest;

import BasePage.BasePages;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.*;

public class BaseTests extends BasePages {

    WebDriver driver = getDriver();

    public WebDriver getDriver(){
        setupDriver();
        return driver;
    }


//    @BeforeClass
//    public void setUp(){
//        driver = getDriver();
//
//
//    }

    @AfterClass
    public static void tearDown(){
        BasePages.tearDown();
    }


}
