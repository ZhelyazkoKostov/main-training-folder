import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Scanner scan = new Scanner(System.in);
        int number = scan.nextInt();
        System.out.println(countEncounter(number));

    }

    public static int countEncounter(int number) {
        if (number < 8) {
            return 0;
        }


        if ((number % 10) == 8 && ((number /10)% 10)==8) {
            return ((number % 10) / 8)*2 + countEncounter(number / 10);
        }
        else if((number % 10) == 8) {
            return ((number % 10) / 8)  + countEncounter(number / 10);
        }
        else{
            return countEncounter(number / 10);
        }


    }
}
