import java.util.Scanner;

public class Main {


    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        String[] line = scan.next().split(",");
        int startPoint = Integer.parseInt(scan.next());
        boolean check = containNumber(line, startPoint);
        System.out.println(check);
    }

    public static boolean containNumber(String[]line, int startPoint){
        if(startPoint==line.length){
            return false;
        }

        if (Integer.parseInt(line[startPoint])==6) {
            return true;
        }
        else {
            startPoint++;
            return containNumber(line, startPoint);
        }

    }
}
