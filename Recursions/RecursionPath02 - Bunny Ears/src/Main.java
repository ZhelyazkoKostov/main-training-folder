import java.util.Scanner;

public class Main {

    public static void main(String [] args){

        Scanner scan = new Scanner(System.in);
        int bunnies =scan.nextInt();
        System.out.println(countBunnyEars(bunnies));

    }

    public static int countBunnyEars(int bunnies){
        if(bunnies<1){
            return 0;
        }
        return ((bunnies/bunnies)*2) + countBunnyEars(bunnies-1);
    }
}
