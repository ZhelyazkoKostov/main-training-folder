import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Scanner scan = new Scanner(System.in);
        String input = scan.next();
        System.out.println(countEncounter(input));


    }
    public static String output = "";

    public static String countEncounter(String input) {


        if (input.length()==0   ) {
            return output;
        }

        if(input.length()==1){return output + input.substring(0,1);}
        else if(input.substring(0,2).equals("pi")){
            return output + "3.14" + countEncounter(input.substring(2,input.length()));
        }

        else{
            return output + input.substring(0,1)+ countEncounter(input.substring(1,input.length()));
        }

    }
}
