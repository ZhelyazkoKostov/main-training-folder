import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Scanner scan = new Scanner(System.in);
        int number = scan.nextInt();
        System.out.println(countEncounter(number));

    }

    public static int countEncounter(int number) {
        if (number < 7) {
            return 0;
        }


        if ((number % 10) == 7) {
            return (number % 10) / 7 + countEncounter(number / 10);
        } else {
            return countEncounter(number / 10);
        }


    }
}
