import java.util.Scanner;

public class Main {

    public static void main(String [] args){

        Scanner scan = new Scanner(System.in);
        int bunnies =scan.nextInt();
        if(bunnies % 2 ==0){
            System.out.println(countEvenBunnyEars(bunnies));}
        else{
            System.out.println(countOddBunnyEars(bunnies));}

    }

    public static int countEvenBunnyEars(int bunnies){
        if(bunnies<1){
            return 0;
        }

        return ((bunnies/bunnies)+2)+countOddBunnyEars((bunnies-1));
    }

    public static int countOddBunnyEars(int bunnies){
        if(bunnies<1){
            return 0;
        }

        return ((bunnies/bunnies)+1)+countEvenBunnyEars((bunnies-1));
    }
}

