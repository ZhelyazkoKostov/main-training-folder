import java.util.Scanner;

public class Main {

    public static void main(String [] args){

        Scanner scan = new Scanner(System.in);
        int number =scan.nextInt();
        System.out.println(factorial(number));

    }

    public static int factorial(int number){
        if(number<=1){
            return 1;
        }
        return number * factorial(number-1);
    }
}
