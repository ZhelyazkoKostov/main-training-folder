package functionalities;

import com.google.common.base.Verify;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import pages.HomePage;
import utils.Utils;

public class ProjectPlan {

    private WebDriver driver;
    private Utils utils = new Utils();
    private HomePage homePage;
    private Actions actions;
    private By table = By.xpath("//ui-table[1]");
    private By iframe = By.id("qa-COMMON_EDITOR_IFRAME");
    private By insertColumnButton = By.xpath("//ui-table[1]/div[2]/div[3]/div/div/div");
    private By editCellMenuButton = By.cssSelector("ui-table div:nth-child(4) > div > div:nth-child(3)");
    private By topCellNewColumn = By.cssSelector("div.container td:nth-child(2) >div");
    private By topCellNewColumnForVerification = By.cssSelector("div.container td:nth-child(2)");
    private By cellRedColor = By.cssSelector("div[style='background-color: rgb(253, 123, 57);']");




    public ProjectPlan(WebDriver driver){
        this.driver = driver;
        homePage = new HomePage(driver);
        actions = new Actions(driver);
    }

    private void insertColumn(){
        homePage.waitElementPresent(utils.getWaitForWaitMethod(),iframe);
        WebElement frame = driver.findElement(iframe);
        driver.switchTo().frame(frame);
        homePage.waitElementPresent(utils.getWaitForWaitMethod(),table);
        WebElement projectTable = driver.findElement(table);
        actions.moveToElement(projectTable)
                .click(projectTable)
                .build()
                .perform();
        homePage.waitElementPresent(utils.getWaitForWaitMethod(),insertColumnButton);
        WebElement newColumnButton = driver.findElement(insertColumnButton);
        actions.moveToElement(newColumnButton)
                .click(newColumnButton)
                .build()
                .perform();
    }

    public boolean newColumnCreated(){
        insertColumn();
        boolean isCellEmpty = false;
        String text = homePage.getDerievedElementText(topCellNewColumn);
        if(text.equals("")){
            isCellEmpty = true;
            System.out.println("The specified cell is empty!");
        }
        else{
            System.out.println("The specified cell contains text: " + text);
        }
        Verify.verify(isCellEmpty);
        return isCellEmpty;
    }

    public boolean changeCellColor(){
        WebElement cell = driver.findElement(topCellNewColumn);
        cell.click();

        WebElement modifyCellMenuBtn = driver.findElement(editCellMenuButton);
        modifyCellMenuBtn.click();

        WebElement redColor = driver.findElement(cellRedColor);
        redColor.click();

        WebElement cellForVerification = driver.findElement(topCellNewColumnForVerification);
        boolean isColorChanged = cellForVerification.getAttribute("style").contains("#fd7b39");
        return isColorChanged;

//        if(cellForVerification.getAttribute("style").contains("#fd7b39")){
//            System.out.println("Cell color changed Successfully!!!");
//        }
//        else {
//            System.out.println("Cell color change Not Successful!!!");
//        }

    }



}
