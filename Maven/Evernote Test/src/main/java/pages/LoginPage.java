package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import utils.Utils;

public class LoginPage {

    private WebDriver driver;
    private By usernameField = By.id("username");
    private By passwordField = By.id("password");
    private By continueButton = By.id("loginButton");
    private String username = "tester@abv.bg";
    private String pwd = "asd123";
    private Utils utils = new Utils();
    private HomePage homePage;


    LoginPage(WebDriver driver){
        this.driver = driver;
        homePage = new HomePage(driver);
    }

    public MainPage login(){
        homePage.typeInField(usernameField, username);
        homePage.clickElement(continueButton);
        homePage.waitElementPresent(utils.getWaitForWaitMethod(),By.xpath("//ol/li[4]"));
        WebElement prdField = driver.findElement(passwordField);
        prdField.sendKeys(pwd);
        homePage.clickElement(continueButton);
        return new MainPage(driver);
    }




}
