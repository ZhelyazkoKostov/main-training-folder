package pages;

import functionalities.ProjectPlan;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import utils.Utils;

public class MainPage{

    private WebDriver driver;

    private By adFrame = By.id("");
    private By adFrameCloseButton = By.id("qa-GENERIC_LIGHTBOX_CLOSE");
    private By workSelectButton = By.xpath("//span[text()=\"Work\"]");
    private By newNoteDropDownMenuButton = By.cssSelector(".dropdown2 > span:nth-child(1)");
    private By schoolSelectButton = By.xpath("//span[text()=\"School\"]");
    private By personalSelectButton = By.xpath("//span[text()=\"Personal\"]");
    private By projectPlanButton = By.xpath("//p[text()=\"Project Plan\"]");
    private By projectPlanButtonFromDropDown = By.xpath("//span[text()=\"Project Plan\"]");
    private By getProjectPlanSideNote = By.id("1_qa-NOTES_SIDEBAR_NOTE_TITLE");
    private By projectPlanCreateNoteRadio = By.xpath("//span[@id=\"qa-CREATE_A_NOTE_MENU_ITEM\"]/*[name()=\"svg\"]/*[name()='circle']");
    private By meetingNoteButton = By.xpath("//p[text()=\"Meeting Note\"]");
    private By toDoButton = By.xpath("//p[text()=\"To-do\"]");
    private By blankNoteButton = By.xpath("//p[text()=\"Blank Note\"]");
    private HomePage homePage;
    private Utils utils = new Utils();
    private Actions actions;



    public MainPage(WebDriver driver){
        this.driver = driver;
        homePage = new HomePage(driver);
        actions = new Actions(driver);
    }

    public ProjectPlan chooseProjectPlanNewUser(){
        homePage.waitElementPresent(utils.getWaitForWaitMethod(),newNoteDropDownMenuButton);
        driver.findElement(newNoteDropDownMenuButton).click();
        homePage.waitElementPresent(utils.getWaitForWaitMethod(),projectPlanButtonFromDropDown);
        driver.findElement(projectPlanButtonFromDropDown).click();
        return new ProjectPlan(driver);
    }

    public ProjectPlan chooseProjectPlanExistingUser(){
        homePage.waitElementPresent(utils.getWaitForWaitMethod(),workSelectButton);
        driver.findElement(workSelectButton).click();
        homePage.waitElementPresent(utils.getWaitForWaitMethod(),projectPlanButton);
        driver.findElement(projectPlanButton).click();
        return new ProjectPlan(driver);
    }



    public boolean verifyProjectPlanCreated(){
        boolean isProPlanCreated;
        homePage.waitElementPresent(utils.getWaitForWaitMethod(),getProjectPlanSideNote);
        isProPlanCreated = homePage.elementPresent(getProjectPlanSideNote);
        return isProPlanCreated;
    }

    private boolean checkIfAdBannerDisplayed(){
        boolean isDisplayed;
        WebElement banner = driver.findElement(adFrame);
        isDisplayed = banner.isDisplayed();
        return isDisplayed;

    }

    public void closeAdBanner(){
        boolean isAdDisplayed = checkIfAdBannerDisplayed();
        if(isAdDisplayed) {
            WebElement closeBtn = driver.findElement(adFrameCloseButton);
            closeBtn.click();
        }
        else {
            System.out.println("Ad has not been displayed!");
        }
    }










}
