package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class SignupPage {

    private WebDriver driver;
    private By usernameField = By.id("email");
    private By passwordField = By.id("passwordInput");
    private By confirmRegistrButton = By.id("register");



    public SignupPage(WebDriver driver){
        this.driver = driver;

    }

    public MainPage register(String username, String password){
        driver.findElement(usernameField).sendKeys(username);
        driver.findElement(passwordField).sendKeys(password);
        driver.findElement(confirmRegistrButton).click();
        return new MainPage(driver);
    }





}
