package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import utils.Utils;


public class HomePage {

    private WebDriver driver;
    private By signupButton = By.xpath("//a[contains(text(),\"Sign up\")]");
    private By loginButton = By.xpath("//a[contains(text(),\"Log in\")]");
    private Utils utils = new Utils();

    public HomePage(WebDriver driver){
        this.driver = driver;
    }


    public SignupPage goToSignUpForm(){
        clickElement(signupButton);
        return new SignupPage(driver);
    }

    public LoginPage goToLoginForm(){
        clickElement(loginButton);
        return new LoginPage(driver);
    }

    public void waitElementPresent(int seconds, By toLoadLocator){
        WebDriverWait wait = new WebDriverWait(driver, seconds);
        wait.until(ExpectedConditions.visibilityOfElementLocated(toLoadLocator));
    }

    public void waitWebElementPresent(int seconds, String path){
        By toLoadLocator = By.xpath(path);
        WebDriverWait wait = new WebDriverWait(driver, seconds);
        wait.until(ExpectedConditions.visibilityOfElementLocated(toLoadLocator));
    }

    void clickElement(By elementName){
        waitElementPresent(utils.getWaitForWaitMethod(),elementName);
        driver.findElement(elementName).click();
    }

    void typeInField(By elementName, String text){
        waitElementPresent(utils.getWaitForWaitMethod(),elementName);
        WebElement element = driver.findElement(By.id("username"));
        element.sendKeys(text);
    }

    void clearField(By elementName){
        waitElementPresent(utils.getWaitForWaitMethod(),elementName);
        WebElement element = driver.findElement(By.id("username"));
        element.sendKeys(Keys.CONTROL,"a");
        element.sendKeys(Keys.BACK_SPACE);
    }

    String derieveElementText(By elementName){
        String text;
        waitElementPresent(utils.getWaitForWaitMethod(), elementName);
        WebElement element = driver.findElement(elementName);
        text = element.getText();
        return text;
    }

    public String getDerievedElementText(By elementName){
        return derieveElementText(elementName);
    }

    boolean elementPresent(By elementName){
        boolean isVisible;
        WebElement element = driver.findElement(elementName);
        isVisible = element.isDisplayed();
        return isVisible;
    }


}
