package base;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.BeforeClass;
import io.github.bonigarcia.wdm.ChromeDriverManager;
import io.github.bonigarcia.wdm.FirefoxDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import pages.HomePage;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

public class BaseTests {

    private WebDriver driver;
    protected HomePage homePage;
    private String browser = getConfigProperties("browser");

    @BeforeClass
    public void setUp(){
        if (browser.equals("firefox")) {
            FirefoxDriverManager.firefoxdriver().setup();
            driver = new FirefoxDriver();
            driver.manage().window().maximize();

        } else if (browser.equals("chrome")) {
            ChromeOptions opts = new ChromeOptions();
            opts.addArguments("-incognito");
            ChromeDriverManager.chromedriver().setup();
            driver = new ChromeDriver(opts);
            driver.manage().window().maximize();

        }
        driver.get(getConfigProperties("url"));

        homePage = new HomePage(driver);


    }

    public String getConfigProperties(String prop){
        Properties properties = new Properties();
        try{
            properties.load(new FileInputStream("src/main/resources/config.properties"));
        }catch (FileNotFoundException e){
            System.out.println(e.getMessage());
        }catch (IOException ex){
            System.out.println(ex.getMessage());
        }
        return properties.getProperty(prop);
    }


    public boolean verifyOnCorrectPage(String expectedTitle, int seconds, By toLoadLocator){
        waitPageLoad(seconds, toLoadLocator);
        String actualTitle = driver.getTitle();
        System.out.println("Actual page title is: " + actualTitle);
        System.out.println("Expected page title is: " + expectedTitle);
        boolean result;
        if (actualTitle.equals(expectedTitle)){
            return result = true;
        }
        else{
            return result = false;
        }

    }

    public void waitPageLoad(int seconds, By toLoadLocator){
        WebDriverWait wait = new WebDriverWait(driver, seconds);
        wait.until(ExpectedConditions.visibilityOfElementLocated(toLoadLocator));
    }



    public void tearDown(){
        driver.quit();
    }



}
