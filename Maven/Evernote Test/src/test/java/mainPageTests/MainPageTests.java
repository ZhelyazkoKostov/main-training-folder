package mainPageTests;

import base.BaseTests;
import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import pages.LoginPage;
import pages.MainPage;

public class MainPageTests extends BaseTests {

    private LoginPage loginPage;
    private MainPage mainPage;

    @BeforeClass
    public void initPage(){
        loginPage = homePage.goToLoginForm();
        mainPage = loginPage.login();
    }


}
