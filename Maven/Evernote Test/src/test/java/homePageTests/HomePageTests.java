package homePageTests;

import base.BaseTests;
import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;
import pages.HomePage;

public class HomePageTests extends BaseTests {

    private By loginButton = By.xpath("//a[contains(text(),\"Sign up\")]");
    private int waitSeconds = 10;

    @Test
    public void verifyOnHomePage(){
        boolean result = verifyOnCorrectPage(getConfigProperties("homePageTitle"), waitSeconds, loginButton);
        Assert.assertTrue(result);
    }

    @AfterClass
    public void closeBrowser(){
        tearDown();
    }

}
