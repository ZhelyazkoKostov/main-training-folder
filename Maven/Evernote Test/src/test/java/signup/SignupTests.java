package signup;

import base.BaseTests;
import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import pages.SignupPage;
import utils.Utils;

public class SignupTests extends BaseTests {

    private By adFrame = By.id("qa-GENERIC_LIGHTBOX_DIALOG");
    private Utils utils = new Utils();
    private String username = "test9@abv.bg";
    private String password = "asdf1234";
    private SignupPage signupPage;

    @BeforeClass
    public void initPage(){
        signupPage = homePage.goToSignUpForm();
    }

    @Test
    public void userSuccessfullySignsUp(){
        signupPage.register(username, password);
        boolean result = verifyOnCorrectPage(getConfigProperties("mainPageTitle"), utils.getWaitForWaitMethod(), adFrame);
        Assert.assertTrue(result);
    }

    @AfterClass
    public void closeBrowser(){
        tearDown();
    }




}
