package functionalities;

import base.BaseTests;
import com.google.common.base.Verify;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import pages.LoginPage;
import pages.MainPage;

public class ProjectPlanTests extends BaseTests {

    private LoginPage loginPage;
    private MainPage mainPage;
    private ProjectPlan projectPlan;

    @BeforeClass
    public void initPage(){
        loginPage = homePage.goToLoginForm();
        mainPage = loginPage.login();
        projectPlan = mainPage.chooseProjectPlanNewUser();
    }

    @Test (priority=1)
    public void userCanCreateProjectPlan(){
        boolean status;
        status = mainPage.verifyProjectPlanCreated();
        Assert.assertTrue(status);

    }

    @Test(priority=2)
    public void projectPlanTableModifiable(){
//      INSERT NEW COLUMN
        boolean isColumnCreated;
        isColumnCreated = projectPlan.newColumnCreated();
        Verify.verify(isColumnCreated);

//      MODIFY CELL COLOR
        boolean isColorChanged;
        isColorChanged = projectPlan.changeCellColor();
        Verify.verify(isColorChanged);

    }

}
