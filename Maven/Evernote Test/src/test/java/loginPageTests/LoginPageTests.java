package loginPageTests;

import base.BaseTests;
import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import pages.LoginPage;
import utils.Utils;

public class LoginPageTests extends BaseTests {

    private LoginPage loginPage;
    private By usernameField = By.id("username");
    private By adFrame = By.className("ReactModalPortal");
    private Utils utils = new Utils();

    @BeforeClass
    public void initPage(){
        loginPage = homePage.goToLoginForm();
    }

    @Test
    public void userOnLoginPage(){
        boolean status = verifyOnCorrectPage(getConfigProperties("loginPageTitle"),10,usernameField);
        Assert.assertTrue(status);
    }

    @Test
    public void userSuccessfulLogin(){
        loginPage.login();
        boolean result = verifyOnCorrectPage(getConfigProperties("mainPageTitle"), utils.getWaitForWaitMethod(), adFrame);
        Assert.assertTrue(result);
    }

    @AfterClass
    public void closeBrowser(){
        tearDown();
    }

}
