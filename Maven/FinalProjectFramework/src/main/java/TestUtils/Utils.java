package TestUtils;

public class Utils {

    private final String CONFIG_FILE_PATH = "C:\\Users\\Administrator\\IdeaProjects\\main-training-folder\\Maven\\FinalProjectFramework\\src\\main\\resources\\config.properties";
    private final String UIMAPPING_FILE_PATH = "C:\\Users\\Administrator\\IdeaProjects\\main-training-folder\\Maven\\FinalProjectFramework\\src\\main\\resources\\ui_mapping.properties";
    private final String LANDING_PAGE_URL = "https://schoolforum.telerikacademy.com";
    public String getCONFIG_FILE_PATH(){
        return CONFIG_FILE_PATH;
    }

    public String getUIMAPPING_FILE_PATH(){
        return UIMAPPING_FILE_PATH;
    }

    public String getLANDING_PAGE_URL(){
        return LANDING_PAGE_URL;
    }
}
