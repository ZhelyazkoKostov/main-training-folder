package BasePage;

import TestUtils.Utils;
import io.github.bonigarcia.wdm.ChromeDriverManager;
import io.github.bonigarcia.wdm.FirefoxDriverManager;
import io.github.bonigarcia.wdm.InternetExplorerDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

public class BasePages {

    public static WebDriver driver;
    private static PropertiesManager propManager = new PropertiesManager();
    public static String locator;
    private static Utils utils = new Utils();


    protected static void setupDriver(){
        String browser = getConfigPropertyByKey("browser");
        if (browser.equals("Firefox")) {
            FirefoxOptions opts = new FirefoxOptions();
            opts.addArguments("-private");
            FirefoxDriverManager.getInstance().setup();
            driver = new FirefoxDriver(opts);
            driver.manage().window().maximize();


        } else if (browser.equals("Chrome")) {
            ChromeOptions opts = new ChromeOptions();
            opts.addArguments("-incognito");
            ChromeDriverManager.getInstance().setup();
            driver = new ChromeDriver(opts);
            driver.manage().window().maximize();


        } else if (browser.equals("Internet Explorer")) {
            DesiredCapabilities capabilities = DesiredCapabilities.internetExplorer();
            capabilities.setCapability(InternetExplorerDriver.FORCE_CREATE_PROCESS, true);
            capabilities.setCapability(InternetExplorerDriver.IE_SWITCHES, "-private");
            InternetExplorerDriverManager.getInstance().setup();
            driver = new InternetExplorerDriver(capabilities);
            driver.manage().window().maximize();

        }
        driver.get(utils.getLANDING_PAGE_URL());

    }

    public static void tearDown(){
        driver.manage().deleteAllCookies();
        driver.close();

    }


    // -------------- GET PROPERTIES METHODS --------------


    public static String getConfigPropertyByKey(String key){
        return locator = propManager.getConfigProperties().getProperty(key);

    }

    public String getUiMappingPropertyByKey(String key){
        return locator = propManager.getUi_mappingProperties().getProperty(key);

    }





}
