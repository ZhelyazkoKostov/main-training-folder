package PageTests;

import BaseTest.BaseTests;
import Pages.LandingPage;
import org.testng.Assert;
import org.testng.annotations.*;

public class LandingPageTests extends BaseTests {


    private LandingPage landingPage;

    @BeforeMethod
    public void initiateLandingPage(){
        setupDriver();
        landingPage = new LandingPage();
    }



    @Test (priority = 2)
    public void verifyOnLandingPage(){
        boolean status = landingPage.isLandingInitialized();
        Assert.assertTrue(status);
    }

    @Test(priority = 1)
    public void verifyPageTitle(){
       landingPage.getTitle();
    }

    @Test(priority = 3)
    public void verifyLoginSuccessful(){
        landingPage.loginWith();
    }


    @AfterMethod
    public void testClosure(){
        driver.close();
    }

}
