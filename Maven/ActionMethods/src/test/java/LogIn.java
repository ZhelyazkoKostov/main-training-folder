import org.junit.BeforeClass;
import org.junit.Test;

public class LogIn extends BaseTest {

    UserMethods umthd = new UserMethods();

//    @BeforeClass
//    public void setUp(){
//
//    }

    @Test
    public void checkLogInPage(){
        umthd.loadBrowser();
        umthd.assertPageTitle("Училищна Телерик Академия");
        umthd.waitElementToBeVisibleByXpath("xpath.logIn",10);
        umthd.clickElementByXpath("xpath.logIn");
        umthd.waitElementToBeVisibleByID("id.signInButton",10);
        umthd.assertPageTitle("Sign in");
    }


}
