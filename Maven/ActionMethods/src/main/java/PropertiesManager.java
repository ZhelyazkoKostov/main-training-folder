import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

public class PropertiesManager {
    public enum PropertiesManagerEnum {
        INSTANCE;
        private final String UI_MAP = "src\\main\\resources\\ui_map.properties";
        private final String CONFIG = "src\\main\\resources\\config.properties";
        private Properties configProperties = null;
        private Properties uiProperties = null;

        private Properties loadProperties(String url) {
            Properties prop = new Properties();
            try {
                FileInputStream fis = new FileInputStream(url);
                prop.load(fis);
            } catch (FileNotFoundException e) {
                //Utils.LOG.error("Loading Properties File Not Found");
            } catch (IOException e) {
                //Utils.LOG.error("Loading properties failed!");
            }
            return prop;
        }

        public Properties getConfigProperties() {
            return configProperties = loadProperties(CONFIG);
        }

        public Properties getUiProperties() {
            return uiProperties = loadProperties(UI_MAP);
        }
    }
}
