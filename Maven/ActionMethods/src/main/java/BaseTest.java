import io.github.bonigarcia.wdm.ChromeDriverManager;
import io.github.bonigarcia.wdm.FirefoxDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;

public class BaseTest {
    public enum BaseTestEnum {
        INSTANCE;
        String browser = PropertiesManager.PropertiesManagerEnum.INSTANCE.getConfigProperties().getProperty("browser");
        WebDriver driver = setupDriver();


        private WebDriver setupDriver() {
            if (browser.equals("chrome")) {
                ChromeOptions opts = new ChromeOptions();
                opts.addArguments("-incognito");
                ChromeDriverManager.getInstance().setup();
                WebDriver driver = new ChromeDriver(opts);
                driver.manage().window().maximize();
                return driver;

            } else if (browser.equals("firefox")) {
                FirefoxOptions opts = new FirefoxOptions();
                opts.addArguments("-private");
                FirefoxDriverManager.getInstance().setup();
                WebDriver driver = new FirefoxDriver(opts);
                driver.manage().window().maximize();
                return driver;
            }
            return driver;
        }

        public WebDriver getDriver() {
            return driver;
        }

        public void quitDriver() {
            if (driver != null) {
                driver.quit();
            }
        }
    }
}



