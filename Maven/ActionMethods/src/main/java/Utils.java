import org.openqa.selenium.WebDriver;

public class Utils {

    private final int EXPLICIT_WAIT = 10;

    public static WebDriver getWebDriver(){
        return BaseTest.BaseTestEnum.INSTANCE.getDriver();
    }

    public void tearDownWebDriver(){
        BaseTest.BaseTestEnum.INSTANCE.quitDriver();
    }

    public String getUIMappingByKey(String key){
        String locator = PropertiesManager.PropertiesManagerEnum.INSTANCE.getUiProperties().getProperty(key);
        return locator;
    }

    public String getConfigPropertiesByKey(String key){
        String locator = PropertiesManager.PropertiesManagerEnum.INSTANCE.getConfigProperties().getProperty(key);
        return locator;
    }public int getEXPLICIT_WAIT(){
        return EXPLICIT_WAIT;
    }



}
