import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class UserMethods {

    Utils utils = new Utils();
    WebDriver driver;

    public UserMethods(){
        driver = BaseTest.BaseTestEnum.INSTANCE.getDriver();
    }

    public void loadBrowser(){
        BaseTest.BaseTestEnum.INSTANCE.getDriver().get(utils.getConfigPropertiesByKey("url"));
    }

    //##### CLICK #####

    public void clickElementByID(String id){
        waitElementToBeVisibleByID(id,utils.getEXPLICIT_WAIT());
        WebElement element = driver.findElement(By.id(utils.getUIMappingByKey(id)));
        element.click();
    }

    public void clickElementByXpath(String xpath){
        waitElementToBeVisibleByXpath(xpath,utils.getEXPLICIT_WAIT());
        WebElement element = driver.findElement(By.xpath(utils.getUIMappingByKey(xpath)));
        element.click();
    }

    //##### WAIT #####

    public void waitElementToBeVisibleByID(String id, int seconds){
        WebDriverWait wait = new WebDriverWait(driver, seconds);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(utils.getUIMappingByKey(id))));
    }

    public void waitElementToBeVisibleByXpath(String xpath, int seconds){
        WebDriverWait wait = new WebDriverWait(driver, seconds);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(utils.getUIMappingByKey(xpath))));
    }

    //##### TYPE #####

    public void typeInElementById (String text, String id){
        waitElementToBeVisibleByID(id,utils.getEXPLICIT_WAIT());
        WebElement element = driver.findElement(By.id(id));
        element.sendKeys(text);
    }

    public void typeInElementByXpath (String text, String xpath){
        waitElementToBeVisibleByXpath(xpath,utils.getEXPLICIT_WAIT());
        WebElement element = driver.findElement(By.id(xpath));
        element.sendKeys(text);
    }

    //##### ASSERT #####

    public void assertPageTitle(String expectedTitle){
        String actualTitle = driver.getTitle();
        Assert.assertEquals(expectedTitle, actualTitle);
    }
}
