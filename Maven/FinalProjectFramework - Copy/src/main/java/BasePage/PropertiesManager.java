package BasePage;

import TestUtils.Utils;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

public class PropertiesManager {

    Utils utils = new Utils();

    public Properties properties;
    public Properties configProperties = null;
    public Properties ui_mappingProperties = null;

    public Properties loadProperties(String path) {
        properties = new Properties();
        try {
            properties.load(new FileInputStream(path));
        } catch (FileNotFoundException e){
            System.out.println("Missing file with path " + path);
        } catch (IOException e) {
            System.out.println("Failed to load file with path " + path);
        }

        return properties;
    }

    public Properties getConfigProperties(){
        return configProperties = loadProperties(utils.getCONFIG_FILE_PATH());
    }

    public Properties getUi_mappingProperties(){
        return ui_mappingProperties = loadProperties(utils.getUIMAPPING_FILE_PATH());
    }
}
