package PageTests;

import BaseTest.BaseTests;
import Pages.HomePage;
import Pages.LandingPage;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class HomePageTests extends BaseTests {

    private LandingPage landingPage;
    private HomePage homePage;

    @BeforeMethod
    public void initiateLandingPage(){
        setupDriver();
        landingPage = new LandingPage();
        homePage = landingPage.loginWith();
    }

    @Test
    public void verifyNewTopicSuccessfullyCreated(){

        homePage.createNewTopic();
    }


    @AfterMethod
    public void testClosure(){
        driver.close();
    }


}
