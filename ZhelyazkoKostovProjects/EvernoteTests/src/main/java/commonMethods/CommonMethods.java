package commonMethods;

import base.BasePages;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import javax.sql.rowset.BaseRowSet;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

public class CommonMethods extends BasePages{
    public enum CommonMethodsEnum {
        INSTANCE;


//    public WebDriver driver = BasePages.getDriver();

    public void loadBrowser(String url){
        driver.get(url);
    }

    public String getConfigProperties(String prop){
        Properties properties = new Properties();
        try{
            properties.load(new FileInputStream("src/main/resources/config.properties"));
        }catch (FileNotFoundException e){
            System.out.println(e.getMessage());
        }catch (IOException ex){
            System.out.println(ex.getMessage());
        }
        return properties.getProperty(prop);
    }

    public void waitElementPresent(By elementName, int seconds){
        WebDriverWait wait = new WebDriverWait(driver, seconds);
        wait.until(ExpectedConditions.visibilityOfElementLocated(elementName));
    }

    public void clickWebElement(By elementName){
        driver.findElement(elementName).click();
    }
    }
}
