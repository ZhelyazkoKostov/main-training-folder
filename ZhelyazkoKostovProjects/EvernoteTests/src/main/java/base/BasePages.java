package base;

import io.github.bonigarcia.wdm.ChromeDriverManager;
import io.github.bonigarcia.wdm.FirefoxDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;

public class BasePages {

    protected static WebDriver driver;
    private static String browser = "chrome";

    private static WebDriver setUp(){
        if (browser.equals("firefox")) {
            FirefoxDriverManager.firefoxdriver().setup();
            driver = new FirefoxDriver();
            driver.manage().window().maximize();
//            return driver;
        } else if (browser.equals("chrome")) {
            ChromeOptions opts = new ChromeOptions();
            opts.addArguments("-incognito");
            ChromeDriverManager.chromedriver().setup();
            driver = new ChromeDriver(opts);
            driver.manage().window().maximize();
//            return driver;
        }
        return driver;
    }

    public static WebDriver getDriver(){
        driver = setUp();
        return driver;
    }


}
