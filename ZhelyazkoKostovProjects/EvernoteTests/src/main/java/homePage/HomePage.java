package homePage;

import base.BasePages;
import commonMethods.CommonMethods;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import signUpPage.SignupPage;

public class HomePage {


    private WebDriver driver = BasePages.getDriver();
    private String url;
    private By signUpButton = By.linkText("Sign up");
    private int waitTime = 10;

    public void goToHomePage(){
        url = CommonMethods.CommonMethodsEnum.getConfigProperties("url");
        commonMethods.loadBrowser(url);
//        return new HomePage();
    }

    public SignupPage goToSignUpPage(){
        commonMethods.waitElementPresent(signUpButton,waitTime);
        commonMethods.clickWebElement(signUpButton);
        return new SignupPage();

    }






}
