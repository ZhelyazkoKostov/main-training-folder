import homePage.HomePage;
import org.testng.annotations.Test;

public class HomePageTests {

    private HomePage homePage = new HomePage();

    @Test
    public void goToSignUpPage(){
        homePage.goToHomePage();
        homePage.goToSignUpPage();
    }
}
